#include <QtNetwork/qhostaddress.h>
#include <glog/logging.h>

#include "moff.h"
#include <QtCore/QCoreApplication>
#include <QtCore/QDateTime>
#include <QtCore/QJsonValue>
#include <QtCore/QPoint>
#include <QtCore/QUrl>
#include <QtCore/QUrlQuery>
#include <QtCore/QUuid>
#include <QtNetwork/QHostAddress>

int main(int argc, char *argv[]) {
  QCoreApplication a(argc, argv);
  google::InitGoogleLogging(argv[0]);
  google::LogToStderr();

  LOG(INFO) << "QTime " << QTime::currentTime();
  LOG(INFO) << "QDate " << QDate::currentDate();
  LOG(INFO) << "QDateTime " << QDateTime::currentDateTime();
  LOG(INFO) << "QUuid " << QUuid::createUuid();
  LOG(INFO) << "QUrl " << QUrl("http://localhost");
  LOG(INFO) << "QUrlQuery " << QUrlQuery("q");
  LOG(INFO) << "QHostAddress " << QHostAddress(QHostAddress::LocalHost);
  return 0;
}
