#pragma once

#include <QJsonValue>
#include <QString>
#include <QVariant>
#include <ostream>
#include <type_traits>
#include <utility>

inline std::ostream &operator<<(std::ostream &o, const QString &s) {
  return o << s.toStdString();
}

template <typename T, typename = void>
struct supports_toString_as_QString : std::false_type {};

template <typename T>
struct supports_toString_as_QString<
    T, std::void_t<decltype(std::declval<const T>().toString())>>
    : std::is_same<decltype(std::declval<const T>().toString()), QString> {
  static_assert(
      !std::is_same<T, QVariant>(),
      "QVariant is not allowed due to implicit consturctor overloads allowing "
      "types that result in a empty string with QVariant::toString(), "
      "please use QVariant::toString() explicitly instead.");
  static_assert(!std::is_same<T, QJsonValue>(),
                "QJsonValue is not allowed as QJsonValue::toString() only "
                "works when constructed from a string, "
                "please use QJsonValue::toString() explicitly instead.");
};

template <typename T>
std::enable_if_t<supports_toString_as_QString<T>::value, std::ostream &>
operator<<(std::ostream &o, const T &t) {
  return o << t.toString();
}
